# Wang Kei James Lee

from floodsystem.stationdata import build_station_list
from floodsystem.geo import stations_by_distance

def run():
    """Requirements for Task 1B"""

    # Build list of stations
    stations = build_station_list()

    # Count number of stations and print
    print("Number of stations: {}".format(len(stations)))

    # Call station_by_distance from geo
    x = stations_by_distance(stations, (52.2053, 0.1218))

    # Print 10 closest and 10 furthest
    print ("10 closest stations from Cambridge:", x[:10])
    print ("10 furthest stations from Cambridge:", x[-10:])

if __name__ == "__main__":
     print("*** Task 1B: CUED Part IA Flood Warning System ***")
     # Run Task1B
     run()