# Wang Kei James Lee

from floodsystem.geo import stations_within_radius
from floodsystem.stationdata import build_station_list

def run():
    """Requirements for Task 1C"""

    # Build list of stations
    stations = build_station_list()

    # Count number of stations and print
    print("Number of stations: {}".format(len(stations)))

    # Call function station_within_radius from geo.py
    x = stations_within_radius(stations, (52.2053, 0.1218), 10)
    
    # Print the results
    print (x)

if __name__ == "__main__":
    print("*** Task 1C: CUED Part IA Flood Warning System ***")
    # Run Task1C
    run()