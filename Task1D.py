# -*- coding: utf-8 -*-
"""
Created on Sat Jan 21 12:34:14 2017

@author: Rose Humphry
"""
from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_with_station, stations_by_river

stations = build_station_list()

def run():
    print("The number of rivers with at least one \
    station is:", " ", len(rivers_with_station(stations)))

    rivers_list = rivers_with_station(stations)
    print ("When placed in a list in alphabetical order\
    the first 10 rivers are:", "", rivers_list[:10])

    rivers_dict = stations_by_river(stations)
    print ("The stations by the River Cam are:", "", rivers_dict["River Cam"])
    print ("The stations by the River Aire are:", "", rivers_dict["River Aire"])
    print ("The stations by the Thames are:", "", rivers_dict["Thames"])

if __name__ == "__main__":
     print("*** Task 1D: CUED Part IA Flood Warning System ***")
     # Run Task1B
     run()