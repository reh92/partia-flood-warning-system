# -*- coding: utf-8 -*-
"""
Created on Sat Jan 21 17:54:57 2017

@author: Rose Humphry
"""
from floodsystem.stationdata import build_station_list
from floodsystem.geo import rivers_by_station_number

stations = build_station_list()

def run():
    print(rivers_by_station_number(stations, 9))

if __name__ == "__main__":
     print("*** Task 1E: CUED Part IA Flood Warning System ***")
     # Run Task1B
     run()