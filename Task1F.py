# -*- coding: utf-8 -*-
"""
Created on Mon Jan 23 16:19:38 2017

@author: Rose Humphry
"""
from floodsystem.station import inconsistent_typical_range_stations
from floodsystem.stationdata import build_station_list

stations = build_station_list()

def run():
    list_stations = inconsistent_typical_range_stations(stations)
    list_stations.sort()
    print(list_stations)

if __name__ == "__main__":
     print("*** Task 1F: CUED Part IA Flood Warning System ***")
     # Run Task1B
     run()