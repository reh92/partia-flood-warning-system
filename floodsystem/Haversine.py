from math import sin, cos, asin, radians

AVG_EARTH_RADIUS = 6371

def hav(centre, lat2, lon2):
	"""This function is used to calculate the Great Circle Distance 
	between two coordinate points on a sphere"""
    
    # Obtain coordinates of Cambridge Center and assigned as lat1 and lon1
	lat1, lon1 = centre

    # Turn lat/lon coordinates into radians
	lat1, lon1, lat2, lon2 = map(radians, (lat1, lon1, lat2, lon2))  

	# Apply the Haversine equation
	lat = lat2 - lat1
	lon = lon2 - lon1
	d = sin(lat * 0.5) ** 2 + cos(lat1) * cos(lat2) * sin(lon * 0.5) ** 2
	h = 2 * AVG_EARTH_RADIUS * asin(d**0.5) 
    
	return h