"""This module contains a collection of functions related to
geographical data.

"""

from floodsystem.utils import sorted_by_key
from floodsystem.Haversine import hav

#The following code enables me to use the list of stations created
#in activity 1a in my function
from floodsystem.stationdata import build_station_list
stations = build_station_list()



def rivers_with_station(stations):
    "This function will, given a list of stations, returns\
    all rivers (by name) with a monitoring station"
    rivers = set()
    for station in stations:
        river = station.river
        rivers.add(river)
        rivers_list = list(rivers)
        rivers_list.sort()
    return rivers_list


def stations_by_river(stations):
    " a function that returns a Python dict (dictionary) that maps\
    river names (the ‘key’) to a list of stations on a given river"
    rivers = rivers_with_station(stations)      
    rivers_dict = dict((river, []) for river in rivers)
    for river, station_list in rivers_dict.items():
        for station in stations:
            if station.river == river:
                station_list.append(station.name)
        station_list.sort()            
                
    return rivers_dict    

def rivers_by_station_number(stations, N):
    "determines the N rivers - where N is the index number\
    with the greatest number of monitoring\
    stations. It should return a list of (river name, number of\
    stations) tuples, sorted by the number of stations. \
    In the case that there are more rivers with the same \
    number of stations as the N th entry, include these rivers \
    in the list"
    station_dict = stations_by_river(stations)
    station_no_list = []
    for river, station_list in station_dict.items():
        station_numb = len(station_list)
        station_no_list.append((river, station_numb))
    sorted_station_list = sorted_by_key(station_no_list, 1, \
                                        reverse = True)
    minimum_station_number = sorted_station_list[N][1]
    highest_station_numbers = sorted_station_list[:N]
    for entry in sorted_station_list[N:]:
        station_number = entry[1]
        if station_number == minimum_station_number:
            highest_station_numbers.append(entry)
    return highest_station_numbers
    
def stations_by_distance(stations, centre):
	"""This function forms a list of tuples containing the 
	(Station name, Station town, Distance from Cambridge),
	sorted in ascending order of distances"""

	# Create empty list
	name_town_distance_list = []

	for station in stations:
    	# Take station coord and name the coordinates point lat2 and lon2
		(lat2, lon2) = station.coord

        # Apply the Haversine function to calculate distance "d"
		d = hav(centre, lat2, lon2)

        # Create a long list of tuples containing (name, town, distance)1
		name_town_distance_list += [(station.name, station.town, float("{0:.6f}".format(d)))]

    # Sort the long list in ascending order of distance using the sort_by_key funtion
	sorted_name_town_distance_list = sorted_by_key(name_town_distance_list, 2, False) 
    
	return (sorted_name_town_distance_list)


def stations_within_radius(stations, centre, r):
	# Create empty list
	name_radius_list = []

	for station in stations:
    	# Take station coord and name the coordinates point lat2 and lon2
		(lat2, lon2) = station.coord

        # Apply the Haversine function to calculate distance
		d = hav(centre, lat2, lon2)

		# If the distance from reference center is within range of r, 
		# Create a list of those stations' names
		if d <= r:
			name_radius_list += [station.name]
		else:
			pass

	# Sorting the list of names in alphabetical order
	sorted_name_radius_list = sorted_by_key(name_radius_list, 0, False)
	
	return (sorted_name_radius_list)
