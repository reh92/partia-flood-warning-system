import numpy as np

def test_polyfit():
    '''A test that simply checks that the polyfit function from numpy works
    as intended. I will put in two arrays and check that the output of the 
    poly coeff is as intended'''
    x = np.array([0.0, 1.0, 2.0, 3.0,  4.0,  5.0])
    y = np.array([0.0, 0.8, 0.9, 0.1, -0.8, -1.0])
    z = np.polyfit(x, y, 4)
    z = list(z)
    assert z == [0.020833333333333384, -0.12129629629629687, -0.17361111111110974, 1.0978835978835984, -0.0039682539682550852]

