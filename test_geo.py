import pytest
from floodsystem.geo import rivers_with_station, stations_by_river, rivers_by_station_number, stations_by_distance, stations_within_radius
from floodsystem.station import MonitoringStation

test_station1 = MonitoringStation("test-s-id-1","test-m-id-1", "station-1", (52.2075, 0.1147), None, "river_1", "town_1")
test_station2 = MonitoringStation("test-s-id-2","test-m-id-2", "station-2", (52.2199, 0.1549), None, "river_1", "town_2")
test_station3 = MonitoringStation("test-s-id-3","test-m-id-3", "station-3", (51.5104, -0.1153), None, "river_2", "town_3")

#For my tests for my three functions in test_geo I have created a list
#of test monitoring stations using the monitoringstation function.
#I have created it so that test_station1 and test_station2 are both on
#the same river (river_1) and test_station3 is on a different river
#(river_3) in order to test the functions to their normal data input.

test = [test_station1, test_station2, test_station3]

def test_rivers_with_station():
    "A function that tests that the rivers_with_station function works,\
    I will put in test stations 1, 2 and three and check that it returns\
    a list of two rivers, river_1 and river_2"
    test_river_list = rivers_with_station(test)
    assert test_river_list == ['river_1', 'river_2']

def test_stations_by_river():
    "A function that tests that the stations_by_river function works,\
    I will put in the test list of test stations and check that it\
    returns a dictionary of river_1 mapped to station-1 and station-2\
    and river_2 mapped to station_3."
    test_rivers_dict = stations_by_river(test)
    assert test_rivers_dict == {'river_1': ['station-1', 'station-2'],\
                                'river_2': ['station-3']}

def test_rivers_by_station_number():
    "A function that tests that the rivers_by_station_number function\
    works, I will put in the test list of station and check that it\
    returns a list of 2 tuples, river_1 with 2 stations, and river_2\
    with 1 station"
    test_river_tuple_list = rivers_by_station_number(test, 1)
    assert test_river_tuple_list == [('river_1', 2), ('river_2', 1)]


def test_stations_by_distance():
    """Requirements for testing stations_by_distance"""
    test_name_town_distance_list = stations_by_distance(test, (52.2053, 0.1218))

    # Assert expected results
    assert test_name_town_distance_list == [("station-1", "town_1", 0.542140), \
                                            ("station-2", "town_2", 2.778757), \
                                            ("station-3", "town_3", 78.966256)]


def test_stations_within_radius():
    """Requirements for testing stations_within_radius"""
    test_name_radius_list = stations_within_radius(test, (52.2053, 0.1218), 1)

    # Assert expected results
    assert test_name_radius_list == ["station-1"]
    
    test_name_radius_list = stations_within_radius(test, (52.2053, 0.1218), 10)
    
    # Assert expected results
    assert test_name_radius_list == ["station-1", "station-2"]
    
    test_name_radius_list = stations_within_radius(test, (52.2053, 0.1218), 100)
    
    # Assert expected results
    assert test_name_radius_list == ["station-1", "station-2", "station-3"]