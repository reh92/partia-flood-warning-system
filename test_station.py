"""Unit test for the station module"""

import pytest
from floodsystem.station import MonitoringStation, inconsistent_typical_range_stations


def test_create_monitoring_station():

    # Create a station
    s_id = "test-s-id"
    m_id = "test-m-id"
    label = "some station"
    coord = (-2.0, 4.0)
    trange = (-2.3, 3.4445)
    river = "River X"
    town = "My Town"
    s = MonitoringStation(s_id, m_id, label, coord, trange, river, town)

    assert s.station_id == s_id
    assert s.measure_id == m_id
    assert s.name == label
    assert s.coord == coord
    assert s.typical_range == trange
    assert s.river == river
    assert s.town == town

def test_inconsistent_typical_range_stations():
    "This function tests that the inconsistent_typical_range function\
    works. Station_1 has no range, station_2 has a consistent range and\
    station_3 has an inconsistent range (the high value is smaller\
    than the low value). I have asserted that the output shall be\
    a list containing station 1 and 3, otherwise an error shall be\
    raised."
    
    test_station1 = MonitoringStation("test-s-id-1","test-m-id-1", "station-1", (1,1), None, "river_1", "town_1")
    test_station2 = MonitoringStation("test-s-id-2","test-m-id-2", "station-2", (2,2), [0,1], "river_1", "town_2")
    test_station3 = MonitoringStation("test-s-id-3","test-m-id-3", "station-3", (3,3), [1,0], "river_2", "town_3")
    test = [test_station1, test_station2, test_station3]
    inconsistent_stations_list = inconsistent_typical_range_stations(test)
    assert inconsistent_stations_list == ['station-1', 'station-3']